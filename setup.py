try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'podium',
    'author': 'Adam Reid',
    'url': 'https://bitbucket.org/epicDistortion/podium',
    'download_url': 'https://bitbucket.org/epicDistortion/podium',
    'author_email': 'adam@adamreid.ca',
    'version': '0.1',
    'install_requires': ["docopt"],
    'packages': ["podium"],
    'entry_points': {'console_scripts': ['pod = podium.podium:main']},
    'name': 'podium',
    'setup_requires': ['pytest-runner'],
    'tests_require': ['pytest']
}

setup(**config)
