'''
Test the Transaction class
'''
import json
from podium.transactions import Transaction

def test_transaction_from_json():
    '''
    Check that trasactions loaded from json produce expected results
    '''
    json_str = ('{"id": 1, "movements":'
               '[{"from": "foo", "to": "bar"}]}')
    txn = Transaction.from_json(json_str)
    assert txn.id == 1
    assert txn.movements == [{'from': 'foo', 'to': 'bar'}]

def test_transaction_json():
    '''
    Check that Transaction produces expected json
    '''
    txn = Transaction([{'from': 'foo', 'to': 'bar'}], transaction_id=1)
    assert json.loads(txn.json) == {"id": 1, "movements": [
                                    {"from": "foo", "to": "bar"}]}
