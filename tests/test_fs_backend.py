import os
import pytest
from podium.filesystem_backend import FileSystemPath, FSPathError

def test_exists(tmpdir):
    fs = FileSystemPath(str(tmpdir))
    assert fs.exists == True

def test_isdir(tmpdir):
    fs = FileSystemPath(str(tmpdir))
    assert fs.isdir == True

def test_isfile(tmpdir):
    testfile = tmpdir.join('testfile')
    testfile.write('foo')
    fs = FileSystemPath(str(testfile))
    assert fs.isfile == True

def test_get_children(tmpdir):
    testfile = tmpdir.join('testfile')
    testfile.write('foo')
    fs = FileSystemPath(str(tmpdir))
    assert fs.children == ['testfile']

def test_get_child(tmpdir):
    testfile = tmpdir.join('testfile')
    testfile.write('foo')
    fs = FileSystemPath(str(tmpdir))
    assert fs.get_child('testfile').path == str(testfile)


def test_add_src_dir_dest_dir(tmpdir):
    a = tmpdir.mkdir('a')
    b = tmpdir.mkdir('b')
    testfile = a.join('testfile')
    testfile.write('foo')
    a_fs = FileSystemPath(str(a))
    b_fs = FileSystemPath(str(b))
    b_fs.add(a_fs)
    assert b_fs.children == ['testfile']

def test_add_src_file_dest_file(tmpdir):
    a = tmpdir.join('a')
    b = tmpdir.join('b')
    a.write('foo')
    b.write('bar')
    a_fs = FileSystemPath(str(a))
    b_fs = FileSystemPath(str(b))
    a_fs.add(b_fs)
    assert a.read() == 'bar'

def test_add_src_dir_dest_not_exists(tmpdir):
    a = tmpdir.join('a')
    b = tmpdir.mkdir('b')
    bf = b.join('testfile')
    bf.write('hello')
    a_fs = FileSystemPath(str(a))
    b_fs = FileSystemPath(str(b))
    assert not a_fs.exists
    assert b_fs.exists
    assert b_fs.children == ['testfile']
    a_fs.add(b_fs)
    assert a_fs.children == ['testfile']

def test_add_src_dir_dest_file(tmpdir):
    a = tmpdir.join('a')
    a.write('hello')
    b = tmpdir.mkdir('b')
    bf = b.join('testfile')
    bf.write('hello')
    a_fs = FileSystemPath(str(a))
    b_fs = FileSystemPath(str(b))
    with pytest.raises(FSPathError):
        a_fs.add(b_fs)

def test_mkdir_strips_leading_slash(tmpdir):
    fs = FileSystemPath(str(tmpdir.join('a')))
    fs.mkdir('/b')
    assert fs.children == ['b']

def test_mkdir_creates_subpaths(tmpdir):
    fs = FileSystemPath(str(tmpdir.join('a')))
    fs.mkdir('/b/c/d')
    assert fs.children == ['b']
    children = []
    for root, dirs, files in os.walk(fs.path):
        children = children + dirs + files
    assert children == ['b', 'c', 'd']

def test_push(tmpdir):
    a = tmpdir.join('a')
    b = tmpdir.mkdir('b')
    bf = b.join('testfile')
    bf.write('hello')
    a_fs = FileSystemPath(str(a))
    b_fs = FileSystemPath(str(b))
    b_fs.push(a_fs)
    assert a_fs.children == ['testfile']

