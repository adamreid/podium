'''
Test the TransactionLog class
'''
import pytest

from podium.transactions import TransactionLog, Transaction

@pytest.fixture
def txlogfile(tmpdir):
    '''
    Create a test transaction log file fixture and return it's path
    '''
    txlogdata = ('{"id": 1, "movements":'
                 '[{"from": "foo", "to": "bar"}]}\n'
                 '{"id": 2, "movements":'
                 '[{"from": "bar", "to": "foo"}]}\n')
    txlogf = tmpdir.mkdir("txlog").join("txlog.log")
    txlogf.write(txlogdata)
    return str(txlogf)

def test_read_log_file(txlogfile):
    '''
    Test that reading a transaction log file produces expected data
    '''
    txlog = TransactionLog(txlogfile)
    assert len(txlog.transactions) == 2
    assert txlog.transactions[0].id == 1
    assert txlog.transactions[0].movements == [{'from': 'foo', 'to': 'bar'}]
    assert txlog.transactions[1].id == 2
    assert txlog.transactions[1].movements == [{'from': 'bar', 'to': 'foo'}]

def test_fetch_transaction(txlogfile):
    '''
    Check that we can fetch a transaction by id from the TransactionLog
    '''
    txlog = TransactionLog(txlogfile)
    assert txlog.fetch(1).id == 1
    assert txlog.fetch(2).id == 2

def test_add_transaction(txlogfile):
    '''
    Make sure that transactions are written to memory and disk, or neither if
    there is an error writing to disk.
    '''
    new_tx = Transaction(3, [{"from": "baz", "to": "qux"}])
    txlog = TransactionLog(txlogfile)
    txlog.add(new_tx)
    assert len(txlog.transactions) == 3

    # read the file into a new instance of TransactionLog to make sure it has
    # been updated.
    txlog2 = TransactionLog(txlogfile)
    assert txlog2.transactions[-1].__dict__ == new_tx.__dict__
    del txlog2

    # Cause a failure when writing to make sure non-persisted transactions don't
    # get added to the transactions list
    non_tx = Transaction(4, [{"from": "qux", "to": "baz"}])
    txlog._log.close()
    with pytest.raises(ValueError):
        txlog.add(non_tx)
        assert txlog.transactions[-1].__dict__ != non_tx.__dict__

def test_path_search(txlogfile):
    '''
    Make sure that path_search can find exact and regex matches in the
    transaction log.
    '''
    txlog = TransactionLog(txlogfile)
    new_tx = Transaction([{"from": "baz", "to": "qux"}], 3)
    txlog.add(new_tx)
    exact_matches = txlog.path_search('foo')
    expr_matches = txlog.path_search('[q].*')
    assert exact_matches == txlog.transactions[0:2]
    assert expr_matches == [txlog.transactions[2]]