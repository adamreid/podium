import os
import pytest
from podium.config import PodiumConfig

CFG = """
foo:
    stages:
        - build:
            - $HOME/projects/foo/foo-$version.tar.gz
            - $HOME/projects/foo/foo-devel-$version.tar.gz
        - testing: $foo_public/testing
        - staging: $foo_public/staging
        - release: $foo_public/release
    vars:
        foo_public: /var/www/pub/foo
        version: 0.0.1
"""
HOME = os.environ.get('HOME', '')

def test_parse(tmpdir):
    p = tmpdir.join('podium.yml')
    p.write(CFG)
    # py.path.local is kinda neat, but just gimme the dang path so I can open
    # the file as python intended.
    path = str(p)
    config = PodiumConfig(path)
    assert config.vars == {'foo_public': '/var/www/pub/foo',
                           'version': '0.0.1',
                           'HOME': HOME}
    build = [HOME + '/projects/foo/foo-0.0.1.tar.gz',
             HOME + '/projects/foo/foo-devel-0.0.1.tar.gz']
    assert config['foo']['stages'][0] == {'build':  build}
    assert config['foo']['stages'][1] == {'testing': '/var/www/pub/foo/testing'}
    assert config['foo']['stages'][2] == {'staging': '/var/www/pub/foo/staging'}
    assert config['foo']['stages'][3] == {'release': '/var/www/pub/foo/release'}


