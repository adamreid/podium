# podium

Staged releases for anything.

Podium is a simple system to help you manage releases through stages. It uses a
simple config file to describe the stages a release goes through in it's
lifecycle.

Let's say you have a product called "foo". Every release for "foo" goes
through 3 separate stages to reach a public release. We will call these tiers
testing, staging and release. Foo has one package that we want to promote
through these stages which we obtain from $HOME/projects/foo/foo-x.y.z.tar.gz.

## The .podium.yml File

A config for foo might look like this:

```yaml
# .podium.yml
---
foo:
 stages:
    - build:
      - $HOME/projects/foo/foo-$version.tar.gz
      - $HOME/projects/foo/foo-devel-$version.tar.gz
    - testing: $foo_public/testing
    - staging: $foo_public/staging
    - release: $foo_public/release
  vars:
    version: 1.0.1
    foo_public: /var/www/foo/pub
```


Each project starts with a project name, in our case *foo*. This name will be
used to refer to the project from the `pod` tool when managing it's releases.

### Stages

Each project must have a `stages` section which is comprised of stage names and
ane or more paths. If more than one path is needed in a stage, make sure you use
the yaml array syntax to define each path.

### Variables

Variables start with a dollar sign and may use any letters, numbers or `_` and
may be resolved to values in one of two ways. The first place that podium looks
for variable definitions is in the operating systems environment. If no
environment variable matches those found in the project then the `vars` section
of the project is consulted. If no definition for the variable is found in the
`vars` section then `pod` will quit with an error.

## Basic usage

With our project defined in the `.podium.yml` file we can now start managing
the releases for our *foo* project. Let's say we've just built a new release
with some bugs fixed and we want to release the fix to **testing**.

We could add a new version of foo to **testing** by running:

```bash
$ pod promote foo from build
```

A `promote` command searches in the project definition's `stages` for the
specfied stage and then copies everything in it to the next stage, **testing**
in our example.

Now the package is available in the testing tier for users to start testing it.
Once testing has finished you probably want to promote it to **staging** for
further testing by a wider audience, or you may want to promote *foo* for
**release**. To do so run:

```bash
$ pod promote foo from testing to release
```

## Stage Ordering

When planning your release stages, write them in the order you want promotions
to follow. When promoting you should always specify which stage to promote from.
Specifying where the promotion is going to is optional, and `pod` will assume
that a promotion means the next stage as listed in the config file.


