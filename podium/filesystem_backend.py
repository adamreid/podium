'''
Manage filesystem based targets.
'''
import os


class FSPathError(BaseException):
    '''
    Thrown when FileSystemPath can not complete an operation.
    '''
    pass

class FileSystemPath(object):
    '''
    A FileSystemPath could be a file or directory, a source or a destination.
    It exists to manage the copying of files from one path to another
    '''
    def __init__(self, path):
        '''
        Creates a new FileSystemPath pointed at `path`
        '''
        self.path = path

    @property
    def exists(self):
        '''
        Returns True if this FileSystemPath really exists on the file system.
        '''
        return os.path.exists(self.path)

    @property
    def isdir(self):
        '''
        Returns True if this FileSystemPath points to a directory.
        '''
        if self.path.endswith('/'):
            return True
        else:
            return self.exists and os.path.isdir(self.path)

    @property
    def isfile(self):
        '''
        Returns True if this FileSystemPath points to a file.
        '''
        return self.exists and os.path.isfile(self.path)

    @property
    def children(self):
        '''
        Returns a list. If this FileSystemPath is a directory that directorys
        children are returned. If this FileSystemPath is a file the list
        returned contains only the path of that file.
        '''
        if self.isfile:
            return [self.path]
        if self.isdir:
            return os.listdir(self.path)

    def get_child(self, child_node):
        '''
        Returns a new FileSystemPath instance pointed at the given child_node.
        '''
        return FileSystemPath(os.path.join(self.path, child_node))

    def add(self, other):
        '''
        Adds the contents of the `other` path to this path. If this or the
        other path end in '/' they are assumed to be directories.

        If this path is a directory and the other path is a directory then the
        contents of the other directory will be copied to this directory. If
        the other path is a file, then the file will be copied to this
        directory.

        If this path is a file then and the other path is a file then the other
        file is copied to this file. If the other path is a directory an
        FSPathError is raised.

        If this path does not exist and the other path is a directory then the
        contents of the other directory are copied to this path after it is
        created as a directory. If the other path is a file then this path is
        created as a file whose contents are that of the other file.

        If niether path exist an FSPathError is raised.
        '''
        if not self.exists and not other.exists:
            raise FSPathError(('Neither {} or {} exist. '
                               'Can not determine what to do')
                               .format(self.path, other.path))
        if not self.exists and self.isdir:
            self.mkdir('')
        if (not self.exists or self.isfile) and other.isfile:
            with open(other.path) as src:
                with open(self.path, 'w') as dst:
                    dst.write(src.read())
        elif self.isdir and other.isfile:
            dst_path = os.path.join(self.path, os.path.basename(other.path))
            with open(dst_path, 'w') as dst:
                with open(other.path) as src:
                    dst.write(src.read())
        elif (not self.exists or self.isdir) and other.isdir:
            if not self.exists:
                os.mkdir(self.path)
            for child in other.children:
                ch = other.get_child(child)
                if ch.isfile:
                    self.add(ch)
                if ch.isdir:
                    self.mkdir(ch.path)
                    self.add(ch)
        elif self.isfile and other.isdir:
            raise FSPathError('Can not copy file {} over directory {}'
                              .format(self.path, other.path))

    def mkdir(self, path):
        '''
        Creates directories relative to this objects path.
        '''
        if path.startswith('/'):
            path = path[1:]
        dst = os.path.join(self.path, path)
        if not os.path.exists(dst):
            os.makedirs(dst)

    def push(self, other):
        '''
        Adds this FileSystemPath to the other.
        '''
        other.add(self)
