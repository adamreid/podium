'''Podium. Easy promotion for your files.

Usage:
    podium promote PROJECT from SRC_STAGE
    podium promote PROJECT from SRC_STAGE to DEST_STAGE

Options:
    -h --help   Show help
'''
import sys
import logging
from docopt import docopt

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('podium')

from config import PodiumConfig
from filesystem_backend import FileSystemPath


def promote(cfg, args):
    '''
    Promote files from SRC_STAGE to DEST_STAGE as provided by command line args.
    '''
    src = None
    dest = None
    if not args['from']:
       log.error('You must provide a SRC_STAGE.')
    try:
        src = cfg.get_project_stage(args['PROJECT'], args['SRC_STAGE'])
        dest = cfg.get_next_stage(args['PROJECT'], args['SRC_STAGE'])
        if args['to']:
            dest = cfg.get_project_stage(args['PROJECT'], args['DEST_STAGE'])
    except KeyError:
        log.error('No such stage %s' % args['DEST_STAGE'])

    for s in src:
        fs_src = FileSystemPath(s)
        for d in dest:
            fs_dest = FileSystemPath(d)
            fs_src.push(fs_dest)


def main():
    try:
        cfg = PodiumConfig('.podium.yml')
    except IOError:
        log.error('Could not load .podium.yml')
    args = docopt(__doc__)

    if args['promote']:
        promote(cfg, args)


if __name__ == '__main__':
    main()
