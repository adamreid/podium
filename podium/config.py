import re
import os
import yaml

class PodiumConfig(object):
    """
    Base class for config sections.
    """
    var_re = re.compile(r'(?<=\$)\w+')
    def __init__(self, path):
        self.config_obj = {}
        # TODO: scope vars to project only
        self.vars = {}
        with open(path) as cfg_file:
            self.config_obj = yaml.load(cfg_file.read())
        for project in self.config_obj.keys():
            self.resolve_vars(self.config_obj[project])

    def __getitem__(self, key):
        return self.config_obj[key]

    def _replace_vars(self, section):
            if type(section) is str:
                if '$' in section:
                    for var in self.var_re.findall(section):
                        if var not in self.vars:
                            self.vars[var] = os.environ.get(var, '')
                        section = self._replace_var(section, var)
            if type(section) is dict:
                for key, value in section.iteritems():
                    section[key] = self._replace_vars(value)
            if type(section) is list:
                for index, element in enumerate(section):
                    section[index] = self._replace_vars(element)
            return section

    def _replace_var(self, string, var):
        return string.replace('$%s' % var, self.vars[var])

    def _get_defined_vars(self, vars_obj):
        """
        Collect variables defined in a 'vars' section
        """
        assert type(vars_obj) is dict
        for key, value in vars_obj.iteritems():
            if not self.vars.get(key, None):
                self.vars[key] = self._replace_vars(value)

    def resolve_vars(self, prj):
        self._get_defined_vars(prj.get('vars', {}))
        for key, value in prj.iteritems():
            prj[key] = self._replace_vars(value)

    def get_project_stage_names(self, project):
        return self[project]['stages'].keys()

    def get_project_stage(self, project, stage):
        for s in self.config_obj[project]['stages']:
            if s.keys()[0] == stage:
                return s.values()

    def get_next_stage(self, project, stage):
        for index, _stage in enumerate(self.config_obj[project]['stages']):
            if _stage.keys()[0] == stage:
                return self.config_obj[project]['stages'][index + 1].values()


