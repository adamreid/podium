'''
Recording a history of transactions allows us to find information about how a
set of files moved or changed over time. It also allows us to implement tools
to undo or redo those changes when required.
'''
import json
import re

class TransactionLog(object):
    '''
    TransactionLog lets you read, write, search and fetch from a list of
    transactions. During initialization the list of recorded transactions is
    populated from an on-disk log file. When adding transactions to the log,
    both the on-disk and in-memory logs are updated or neither are.
    '''

    def __init__(self, logpath=None):
        self.transactions = []
        self.current_id = 0
        self.path = logpath
        if not self.path:
            self.path = 'podium.txlog'
        with open(logpath, 'r') as log:
            for line in log.readlines():
                self.transactions.append(Transaction.from_json(line))
        self._log = open(logpath, 'a+')
        if len(self.transactions) > 0:
            self.current_id = self.transactions[-1].id

    def __del__(self):
        """
        Make sure we don't leak file handles.
        """
        self._log.close()

    def add(self, transaction):
        '''
        Adds the given Transaction to the end of the transaction log.

        When you add to the TransactionLog the transaction is first written to
        disk, then appended to the transactions list in-memory. If an error
        occurs during the disk operation the transaction will not be appended to
        the in-memory transactions list and the exeption will be raised to the
        caller to handle.
        '''
        #transaction.id = self.current_id + 1
        self._log.writelines(transaction.json)
        self.transactions.append(transaction)
        self._log.flush()
        #self.current_id += 1

    def fetch(self, transaction_id):
        '''
        Return a transaction with the given transaction_id.
        '''
        for transaction in self.transactions:
            if transaction.id == transaction_id:
                return transaction

    def path_search(self, path_expression):
        '''
        Search in the transaction log for operations on paths matching
        path_expression.
        '''
        matches = []
        for transaction in self.transactions:
            path_list = ' '.join([' '.join(m.values()) for m in
                                    transaction.movements])
            if re.search(path_expression, path_list):
                matches.append(transaction)
        return matches


class Transaction(object):
    '''
    Transaction has an id and a list of movements for the transaction which
    record what happened during a transaction, such as a file copied from one
    path to another.
    '''
    def __init__(self, movements, transaction_id=0):
        self.id = transaction_id
        self.movements = movements

    @property
    def json(self):
        '''
        Returns this transaction as a json string
        '''
        return json.dumps(self.__dict__)

    @staticmethod
    def from_json(json_string):
        '''
        Returns a new Transaction from a json string. Returns None if the json
        string is invalid.
        '''
        obj = json.loads(json_string)
        if 'id' in obj.keys() and 'movements' in obj.keys():
            return Transaction(obj['movements'], transaction_id=obj['id'])
        else:
            return Transaction([])

class TransactionRunner(object):
    '''
    Executes and records a set of movements so that they are either all
    completed, or rolled back.
    '''
    def __init__(self):
        self.movements = []
        self.tx_log = TransactionLog()

    def run(self, source, dest):
        '''
        Run a transaction forward by calling dest.add(source)
        '''
        # TODO: Don't assume targets will always be FileSystemPath's.
        for s in source:
            fs_src = FileSystemPath(s)
            for d in dest:
                fs_dest = FileSystemPath(d)
                fs_dest.add(fs_src)
                self.movements.append({'from': s, 'to': d})

        self.tx_log.add(Transaction(self.movements))
        self.movements = []

     # TODO: implement undo.